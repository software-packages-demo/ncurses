# ncurses

Terminal handling. [invisible-island.net/ncurses](https://invisible-island.net/ncurses)

* [*tset(1) - Linux man page*
  ](https://linux.die.net/man/1/tset)
* [*term(7) - Linux man page*
  ](https://linux.die.net/man/7/term)
